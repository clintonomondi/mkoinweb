<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>M-Koin</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/logo3.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

<!--    end alert-->
  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="img/logo3.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#clients">Tariffs</a></li>
          <li><a href="#contact">Contact Us</a></li>
          <li class="drop-down"><a href="">Account</a>
            <ul>
                <li><a href="http://mkoin.co.ke/home/user/login.html" >Login</a></li>
              <li><a href="signup/individual.html">Individual Sign up</a></li>
              <li><a href="signup">Merchant Sign up</a></li>
            </ul>
          </li>
          <li><a href="#" data-toggle="modal" data-target="#faqs">FAQs</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
<!--    <div class="container">-->
<!--      <div class="intro-info" >-->
<!--        <h2>We provide payment<br><span>solutions</span><br>for you and your business!</h2>-->
<!--        <div>-->
<!--          <a href="signup" class="btn-get-started scrollto" >Join Now</a>-->
<!--          <a href="#about" class="btn-services scrollto">About us</a>-->
<!--        </div>-->
<!--      </div>-->
        <div id="demo" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/banner8.png" alt="M-koin" width="1100" height="500">
                    <div class="carousel-caption">
<!--                        <h3 style="color: black;">M-Koin</h3>-->
<!--                        <p style="color: black;">The Safest way to store and save your loose change</p>-->
<!--                        <p style="color: black;">Dial *626# to  register or get it in PlayStore</p>-->
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="img/banner9.png" alt="M-Koin" width="1100" height="500">
                </div>
<!--                <div class="carousel-item">-->
<!--                    <img src="img/banner3.jpeg" alt="New York" width="1100" height="500">-->
<!--                </div>-->
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
<!--    </div>-->
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
      <section id="about">
          <div class="container">
              <header class="section-header">
                  <h3>About Us</h3>
                  <p> M-Koin is a platform for cashless loose change transactions and also payment of fare to Matatus.
                      It also allows users to buy airtime, Kenya Power tokens and also vouchers.We provide payment solutions for you and your business</p>
              </header>
              <div class="row about-container">
                  <div class="col-lg-6 background order-lg-1 order-2 wow fadeInUp">
                      <h4>M-KOIN App</h4>
                      <p>
                          The <a href="https://play.google.com/store/apps/details?id=ke.co.mkoin.mkoin">M-Koin App </a> is an easy way to buy voice and data bundles while helping you keep track of what
                          you're using and see how much data you are using in your account.
                          It is available on <a href="https://play.google.com/store/apps/details?id=ke.co.mkoin.mkoin">PlayStore </a>  for Android (Operating System version 4.6 or more recent).
                      </p>
                      <h6>Requirements</h6>
                      <ul>
                          <li>Android Operating System</li>
                          <li>M-Koin Account</li>
                      </ul>
                      <h6>Download the M-Koin App Now! </h6>
                      <a href="https://play.google.com/store/apps/details?id=ke.co.mkoin.mkoin"><img src="img/banner/android.png" class="img-fluid" alt=""></a>
                  </div>

                  <div class="col-lg-6 content order-lg-2 order-1">
                      <h4>M-Koin offers variety of services to users</h4>
                      <div class="icon-box wow fadeInUp">
                          <div class="icon"><i class="fa fa-credit-card"></i></div>
                          <h4 class="title"><a href="">Buy Credit</a></h4>
                          <p class="description">M-Koin enables one to buy credit for all network from your M-Koin account to your number and to any other number.</p>
                      </div>

                      <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">.
                          <div class="icon"><i class="fa fa-lightbulb-o"></i></div>
                          <h4 class="title"><a href="">Buy KPLC Token</a></h4>
                          <p class="description">Pay for electricity tokens through M-Koin using your phone or USSD over the internet or at agent points .</p>
                      </div>

                      <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
                          <div class="icon"><i class="fa fa-money"></i></div>
                          <h4 class="title"><a href="">Money Transfer</a></h4>
                          <p class="description">We offer money tranfer services from Mkoin to Mkoin and other unregistered users and m-Koin to Mpesa.</p>
                      </div>
                  </div>

              </div>
          </div>
      </section><!-- #about -->
      <section id="testimonials" class="section-bg">
          <div class="container">

<!--              <header class="section-header">-->
<!--                  <h3>Testimonials</h3>-->
<!--              </header>-->

              <div class="row justify-content-center">
                  <div class="col-lg-8">

                      <div class="owl-carousel testimonials-carousel wow fadeInUp">

                          <div class="testimonial-item">
                              <img src="img/banner/credit/safaricom.jpeg" class="testimonial-img" alt="">
                              <h3>Safaricom Airtime</h3>
<!--                              <h4>Ceo &amp; Founder</h4>-->
                              <p>
                                  To buy airtime just press the Buy Airtime button on the bottom of the dashboard screen and enter the amount to buy then press send. To buy for other phone,
                                  just enter the mobile number and the amount and then press send.
                              </p>
                          </div>

                          <div class="testimonial-item">
                              <img src="img/banner/credit/airtel.jpeg" class="testimonial-img" alt="">
                              <h3>Airtel Airtime</h3>
<!--                              <h4>Designer</h4>-->
                              <p>
                                  To buy airtime just press the Buy Airtime button on the bottom of the dashboard screen and enter the amount to buy then press send. To buy for other phone,
                                  just enter the mobile number and the amount and then press send.
                              </p>
                          </div>

                          <div class="testimonial-item">
                              <img src="img/banner/credit/telcom.jpeg" class="testimonial-img" alt="">
                              <h3>Telkom Airtime</h3>
<!--                              <h4>Store Owner</h4>-->
                              <p>
                                  To buy airtime just press the Buy Airtime button on the bottom of the dashboard screen and enter the
                                  amount to buy then press send. To buy for other phone, just enter the mobile number and the amount and then press send.
                              </p>
                          </div>

                          <div class="testimonial-item">
                              <img src="img/banner/credit/kenyapower.jpeg" class="testimonial-img" alt="">
                              <h3>KPLC Tokens</h3>
<!--                              <h4>Freelancer</h4>-->
                              <p>
                                  Press on the KPLC token button at the bottom of the dashboard screen and then enter your meter number then the amount you wish to purchase and confirm the details and then press okay. You will receive a message with the number of units received. Please note that all the meters that you have used will be saved for future use. You can also view all your previous tokens history when you press the KPLC tokens button and go to History tab.
                              </p>
                          </div>

                      </div>

                  </div>
              </div>


          </div>
      </section><!-- #testimonials -->
    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>Why choose M-Koin?</h3>
          <p>M-koin is made for today and tomorrow, all parts are created with scalability in consideration,
            this enable M-koin to Adopt changing environment without disrupting the current system environment.
            Advanced analytics and reporting:</p>
<!--           <p> Be in the know at all time, we customize real-time reports as required by the-->
<!--            institution. View all payment reports, with deep analytical and comparative view. View performance in comparative to-->
<!--            previous day or week. Drill down to employee performance and regional or sections. .</p>-->
        </header>

        <div class="row row-eq-height justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-users"></i>
              <div class="card-body">
                <h5 class="card-title">It is Available to users in multiple channels: </h5>
                <p class="card-text">The solution can be accessed by users/payers through various channels: </p>
                  <p>Mobile app<br>
                  USSD<br>
                  Web app<br>
                    Agents</p>
                <!--<a href="#" class="readmore">Read more </a>-->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-money"></i>
              <div class="card-body">
                <h5 class="card-title">Multiple Wallet</h5>
                <p class="card-text">The solution has Multiple wallet funding through:</p>
                <p>Mobile Money<br>
                  Banks<br>
                  Agents</p>
                <!--<a href="#" class="readmore">Read more </a>-->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-object-group"></i>
              <div class="card-body">
                <h5 class="card-title">Free M-Koin to M-Koin account transactions</h5>
                <p class="card-text">M-Koin enables users to transact as low as KSh 1 and as much as Ksh 70,000 per single transaction free of charge</p>
                <!--<a href="#" class="readmore">Read more </a>-->
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>

    <!--==========================
      Portfolio Section
    ============================-->


    <!--==========================
      Clients Section
    ============================-->


    <!--==========================
      Team Section
    ============================-->

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="section-bg">
        <div class="section-header">
          <h3>Our Tariffs</h3>
          <!--<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque dere santome nida.</p>-->
        </div>

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">
         <div class="container">
          <div class="row justify-content-center">
            <div class="col-sm-6 mb-4">
              <button class="btn btn-primary btn-sm btn-block" data-toggle="collapse" title="Click to view other Mkoin Trasanctions!" data-target="#demo2">Other M-Koin trasactions from M-Koin wallet</button>
              <div id="demo2" class="collapse">
                  <div id="tab1">
                      <h2>Other M-Koin trasactions from M-Koin wallet</h2>
                <table id="demo1" class="table table-hover table-bordered">
                  <thead style="background-color: maroon; color: black">
                  <tr>
                    <th>Range</th>
                    <th>Amount</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>Send to M-koin User</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>Lipa Fare</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>Buy Airtime</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>Buy KPLC Token</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>Vourchers</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>Lipa Bills</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>Donate</td>
                    <td>0</td>
                  </tr>
                  </tbody>
                    <tfoot>
                    <tr style="background-color: lightgray">
                        <td></td>
                  <td> <a type="button" class="btn btn-info btn-sm fa fa-print" id="btPrint1" onclick="createPDF1()"></a> </td>
                    </tr>
                    </tfoot>
                </table>
                  </div>
              </div>
            </div>
            <div class="col-sm-6">
              <button class="btn btn-primary btn-sm btn-block" data-toggle="collapse" title="Click to view bank withdrawal using pesalink!" data-target="#demo3">M-koin to Bank using PesaLink withdawal tariff</button>
              <div id="demo3" class="collapse">
                  <div id="tab2">
                      <h2>M-koin to Bank  withdawal tariff</h2>
                <table id="demo2" class="table table-hover table-bordered">
                  <thead style="background-color: maroon; color: black">
                  <tr>
                    <th>Range</th>
                    <th>Amount</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>Ksh 0-500</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>Ksh 501-5,000</td>
                    <td>20</td>
                  </tr>
                  <tr>
                    <td>Ksh 5,001-10,000</td>
                    <td>30</td>
                  </tr>
                  <tr>
                    <td>Ksh 10,001-20,000</td>
                    <td>40</td>
                  </tr>
                  <tr>
                    <td>Ksh 20,001-50,000</td>
                    <td>60</td>
                  </tr>
                  <tr>
                    <td>Ksh 50,001-100,000</td>
                    <td>80</td>
                  </tr>
                  <tr>
                    <td>Ksh 100,001-200,000</td>
                    <td>120</td>
                  </tr>
                  <tr>
                    <td>200,001-500,000</td>
                    <td>180</td>
                  </tr>
                  <tr>
                    <td>Ksh 500,001-1,000,000</td>
                    <td>200</td>
                  </tr>
                  </tbody>
                    <tfoot>
                    <tr style="background-color: lightgray">
                        <td></td>
                        <td> <a type="button" class="btn btn-info btn-sm fa fa-print" id="btPrint2" onclick="createPDF2()"></a> </td>
                    </tr>
                    </tfoot>
                </table>
              </div>
            </div>
          </div>
          </div>
<hr>
          <div class="row justify-content-center">
            <div class="col-sm-6 mb-4">
              <button class="btn btn-primary btn-sm btn-block" data-toggle="collapse" title="Click to view mpesa withdrawal!" data-target="#demo1">M-Koin to Mpesa Withdrawal Tariff</button>
              <div id="demo1" class="collapse">
                  <div id="tab3">
                      <h2>M-Koin to Mpesa Withdrawal Tariff</h2>
                <table id="demo2" class="table table-hover table-bordered">
                  <thead style="background-color: maroon; color: black">
                  <tr>
                    <th>Range</th>
                    <th>Amount</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>Ksh 0-1,000</td>
                    <td>30</td>
                  </tr>
                  <tr>
                    <td>Ksh 1,001-10,000</td>
                    <td>40</td>
                  </tr>
                  <tr>
                    <td>Ksh 10,001-30,000</td>
                    <td>50</td>
                  </tr>
                  <tr>
                    <td>Ksh 30,001-70,000</td>
                    <td>70</td>
                  </tr>
                  </tbody>
                    <tfoot>
                    <tr style="background-color: lightgray">
                        <td></td>
                        <td> <a type="button" class="btn btn-info btn-sm fa fa-print" id="btPrint3" onclick="createPDF3()"></a> </td>
                    </tr>
                    </tfoot>
                </table>
              </div>
            </div>
            </div>
            <div class="col-sm-6">
              <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="collapse"  title="Click to view agent tariff!" data-target="#demo">M-koin Customer Withdrawal Tariff</button>
              <div id="demo" class="collapse">
                  <div id="tab4">
                      <h2>M-koin Customer Withdrawal Tariff</h2>
                <table class="table table-hover table-bordered">
                  <thead style="background-color: maroon; color: black">
                  <tr>
                    <th>Transaction Limits</th>
                    <th>M-Koin withdrawal charges</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>50-100</td>
                    <td>7</td>
                  </tr>
                  <tr>
                    <td>101-500</td>
                    <td>19</td>
                  </tr>
                  <tr>
                    <td>501-2,500</td>
                    <td>20</td>
                  </tr>
                  <tr>
                    <td>2,500-3,500</td>
                    <td>35</td>
                  </tr>
                  <tr>
                    <td>3,501-5,000</td>
                    <td>47</td>
                  </tr>
                  <tr>
                    <td>5,001-7,500</td>
                    <td>59</td>
                  </tr>
                  <tr>
                    <td>7,501-10,000</td>
                    <td>78</td>
                  </tr>
                  <tr>
                    <td>10,001-15,000</td>
                    <td>113</td>
                  </tr>
                  <tr>
                    <td>15,001-20,000</td>
                    <td>126</td>
                  </tr>
                  <tr>
                    <td>20,001-35,000</td>
                    <td>134</td>
                  </tr>
                  <tr>
                    <td>35,001-50,000</td>
                    <td>189</td>
                  </tr>
                  <tr>
                    <td>50,001-70,000</td>
                    <td>210</td>
                  </tr>
                  </tbody>
                    <tfoot>
                    <tr style="background-color: lightgray">
                        <td></td>
                        <td> <a type="button" class="btn btn-info btn-sm fa fa-print" id="btPrint4" onclick="createPDF4()"></a> </td>
                    </tr>
                    </tfoot>
                </table>
              </div>
            </div>
          </div>
         </div>
          </div>
          </div>
    </section>

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Contact Us</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
              <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3988.8183844233727!2d36.81395896475392!3d-1.2827919490643678!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sview+pack+towers+nairobi!5e0!3m2!1sen!2ske!4v1558110020704!5m2!1sen!2ske" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                  <p>Utalii Lane,ViewPark Towers 2<sup>nd</sup> floor</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p>info@mkoin.co.ke</p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p>+254 725222221</p>
              </div>
            </div>

            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form id="submitEnquiry"  onsubmit="return false">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Your phone no" data-rule="phone" data-msg="Please enter a valid phone" required  />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="title" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject"  required />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"  required></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->
      <!-- The Modal -->
      <div class="modal fade" id="myModal3">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">

                  <!-- Modal Header -->
                  <div class="modal-header">
                      <h4 class="modal-title">Please wait...</h4>
                  </div>

                  <!-- Modal body -->
                  <div class="modal-body">
                      <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div>
                      </div>
                  </div>

              </div>
          </div>
      </div>


  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>M-Koin</h3>
            <p>View Tech is payment gateway company that offers money transfers, money remittance and Revenue
              collection. We also offer customizable solutions with a cutting edge enforcing mechanism.</p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#about">About us</a></li>
              <li><a href="#services">Services</a></li>
              <li><a href="#clients">Tariffs</a></li>
              <li><a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              Utalii Lane,ViewPark Towers 2<sup>nd</sup> floor <br>
              Nairobi, Kenya <br>
              <strong>Phone:</strong> +254 725222221<br>
              <strong>Email:</strong> info@mkoin.co.ke<br>
            </p>

            <div class="social-links">
              <a href="https://twitter.com/MkoinKe/" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/mkoinke/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://instagram.com/mkoin_kenya" class="instagram"><i class="fa fa-instagram"></i></a>
            </div>

          </div>


        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>M-Koin</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->
        Design <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->
  <?php
  require_once ('modals/signup.php');
  require_once ('modals/faqs.php');
  ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script>
      (function(d,t,u,s,e){e=d.getElementsByTagName(t)[0];s=d.createElement(t);s.src=u;s.async=1;e.parentNode.insertBefore(s,e);})(document,'script','//mkoin.co.ke/help/livechat/php/app.php?widget-init.js');
  </script>
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="signup/js/notify.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  <script src="js/process.js"></script>
  <script src="js/print.js"></script>

  <script>
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });
  </script>
</body>
</html>
