/**
 * Created by pc on 5/18/2019.
 */


$( document ).ready(function() {
    submitEnquiry();
    submitIndividual();
    submitMerchant();
    getCounties();
});

function submitEnquiry(){
    $(document).on('submit', '#submitEnquiry', function(event){
        event.preventDefault();
        $('#myModal3').modal('show');
        var data;
        data = new FormData(this);
        data.append("action", 'submitEnquiry');
        $.ajax({
            url:"api/",
            method:"POST",
            data: data,
            contentType:false,
            processData:false,
            success:function(data)
            {
                console.log(data.message);
                if(data.status)
                {
                    $('#myModal3').modal('hide');
                    $.notify(data.message, "success");
                    $(':input','#submitEnquiry')
                        .not(':button,:submit,:hidden')
                        .val('')
                        .prop('checked',false)
                        .prop('selected',false);
                }
                else
                {
                    $('#myModal3').modal('hide');
                    $.notify(data.message, "error");
                }

            }
        });
    });
}

function submitIndividual(){
    $(document).on('submit', '#submitIndividual', function(event){
        event.preventDefault();
        $('#myModal2').modal('show');
        var data;
        data = new FormData(this);
        data.append("action", 'signUpUser');
        data.append("token", '0');
        data.append("type", '0');
        $.ajax({
            url:"https://mkoin.co.ke/desktopSandbox/",
            method:"POST",
            data: data,
            contentType:false,
            processData:false,
            success:function(data)
            {
                console.log(data.message);

                if(data.status)
                {
                    $('#myModal2').modal('hide');
                    $.notify(data.message, "success");
                    $(':input','#submitIndividual')
                        .not(':button,:submit,:hidden')
                        .val('')
                        .prop('checked',false)
                        .prop('selected',false);
                }
                else
                {
                    $('#myModal2').modal('hide');
                    $.notify(data.message, "error");
                }

            }
        });
    });
}

function submitMerchant(){
    $(document).on('submit', '#submitMerchant', function(event){
        event.preventDefault();
         // alert("hello merchant");
        $('#myModal').modal('show');
        var data;
        data = new FormData(this);
        data.append("action", 'submitMerchant');
        $.ajax({
            url:"../api/",
            method:"POST",
            data: data,
            contentType:false,
            processData:false,
            success:function(data)
            {
                console.log(data);

                if(data.status)
                {
                    $('#myModal').modal('hide');
                    $.notify(data.message, "success");
                    $(':input','#submitMerchant')
                        .not(':button,:submit,:hidden')
                        .val('')
                        .prop('checked',false)
                        .prop('selected',false);
                }
                else
                {
                    $('#myModal').modal('hide');
                    $.each(data.error,function (i,e) {
                        $.notify(data.error[i], "error");
                    });
                }

            }
        });
    });
}

function getCounties(){
    $('#myModal').modal('show');
        var data=[];
        $.ajax({
            url:"../api?"+"action=getCounties",
            method:"GET",
            data: data,
            contentType:false,
            processData:false,
            success:function(data)
            {
                console.log(data);
                for (var i=0;i<data.length;i++ ){
                    $('#county').append('<option value='+data[i].code+'>'+data[i].name+'</option>');
                }
                $('#myModal').modal('hide');
            }
        });
}



