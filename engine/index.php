<?php

/**
 * Created by PhpStorm.
 * User: CLINTON
 * Date: 5/28/2019
 * Time: 4:40 PM
 */
class index
{

    protected $Db;


    public function __construct()
    {
        require 'config.php';
        $Config = new Database();
        $this->Db = $Config->ConnectToDatabase();

    }
   public function submitEnquiry(){
 $sql=$this->Db->prepare('INSERT INTO inquiries( name, phone, title, message) 
VALUES (:name,:phone,:title,:message)');
 $sql->bindParam(':name',$_POST['name'],PDO::PARAM_STR);
 $sql->bindParam(':phone',$_POST['phone'],PDO::PARAM_STR);
 $sql->bindParam(':title',$_POST['title'],PDO::PARAM_STR);
 $sql->bindParam(':message',$_POST['message'],PDO::PARAM_STR);
 if($sql->execute()){
     return ['status'=>true,'message'=>'Your messaged has been submitted successfully'];
 }
 else{
     return ['status'=>false,'message'=>'Could not submit your message'];
 }

    }


    public function submitMerchant(){
        $errors=[];
        $maxsize    = 2097152;
        $acceptable = array(
            'application/pdf',);
        $fileName = $_FILES['kra']['name'];
        $target = "../kra/";
        $fileTarget = $target.$fileName;
        $tempFileName = $_FILES["kra"]["tmp_name"];

        $fileName2 = $_FILES['cr12']['name'];
        $target2 = "../cr12/";
        $fileTarget2 = $target2.$fileName2;
        $tempFileName2 = $_FILES["cr12"]["tmp_name"];

        if(!isset($_POST['fname']) || !isset($_POST['location']) || !isset($_POST['email']) || !isset($_POST['phone']) || !isset($_POST['idno']) || !isset($_POST['building']) || !isset($_POST['county']) || !isset($_POST['box']) || !isset($_POST['branches']) || !isset($_POST['wing']) || !isset($_POST['street'])){
            array_push($errors,'The resource that you are looking for could not be found.');
        }
        $newsphone1 = substr($_POST['phone'], -9);
        $newsphone='254'.$newsphone1;
        $sql=$this->Db->prepare('INSERT INTO agents( fname, location, building, floor,street,wing,kra,cr12,branches,box,county,email,phone,idno,latitude,longitude) 
VALUES (:fname,:location,:building,:floor,:street,:wing,:kra,:cr12,:branches,:box,:county,:email,:phone,:idno,:latitude,:longitude)');
        $sql->bindParam(':fname',$_POST['fname'],PDO::PARAM_STR);
        $sql->bindParam(':location',$_POST['location'],PDO::PARAM_STR);
        $sql->bindParam(':building',$_POST['building'],PDO::PARAM_STR);
        $sql->bindParam(':floor',$_POST['floor'],PDO::PARAM_STR);
        $sql->bindParam(':street',$_POST['street'],PDO::PARAM_STR);
        $sql->bindParam(':wing',$_POST['wing'],PDO::PARAM_STR);
        $sql->bindParam(':branches',$_POST['branches'],PDO::PARAM_STR);
        $sql->bindParam(':box',$_POST['box'],PDO::PARAM_STR);
        $sql->bindParam(':county',$_POST['county'],PDO::PARAM_STR);
        $sql->bindParam(':email',$_POST['email'],PDO::PARAM_STR);
        $sql->bindParam(':phone',$newsphone,PDO::PARAM_STR);
        $sql->bindParam(':idno',$_POST['idno'],PDO::PARAM_STR);
        $sql->bindParam(':latitude',$_POST['latitude'],PDO::PARAM_STR);
        $sql->bindParam(':longitude',$_POST['longitude'],PDO::PARAM_STR);
        $sql->bindParam(':kra',$fileTarget,PDO::PARAM_STR);
        $sql->bindParam(':cr12',$fileTarget2,PDO::PARAM_STR);

        if(count($errors)>0)
        {
            return ['status'=>false,'error'=>$errors];
        }
        else {
            if(strlen($_POST['fname'])<3)
            {
                array_push($errors,'Invalid  name');
            }
            if(strlen($_POST['location'])<3)
            {
                array_push($errors,'Invalid Last  Location');
            }
            if(strlen($_POST['idno'])<7)
            {
                array_push($errors,'Invalid ID No');
            }
            if(strlen($_POST['building'])<4)
            {
                array_push($errors,'Invalid building');
            }
            if(strlen($_POST['phone'])<10)
            {
                array_push($errors,'Invalid phone number');
            }
            if(strlen($_POST['location'])<4)
            {
                array_push($errors,'Invalid Location');
            }

            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                array_push($errors,'Invalid email address');
            }
            if(!is_numeric($_POST['phone'])){
                array_push($errors,'Invalid phone number');
            }
            if (file_exists($fileTarget)) {
                array_push($errors,'The KRA document already exist.');
            }
            if (file_exists($fileTarget2)) {
                array_push($errors,'The Company registration document already exist.');
            }

            if(($_FILES['cr12']['size'] >= $maxsize) || ($_FILES["cr12"]["size"] == 0)) {
                array_push($errors,'The Company Registration File cannot be more than 2MB');
            }
            if(($_FILES['cr12']['size'] >= $maxsize) || ($_FILES["kra"]["size"] >= $maxsize)) {
                array_push($errors,'The  Files cannot be more than 2MB');
            }
            if(!in_array($_FILES['cr12']['type'], $acceptable) && (!empty($_FILES["cr12"]["type"]))) {
                array_push($errors,'The  Company Registration must be PDF format');
            }
            if(!in_array($_FILES['kra']['type'], $acceptable) && (!empty($_FILES["kra"]["type"]))) {
                array_push($errors,'The  KRA file must be PDF format');
            }
//            if(empty($_POST['terms'])){
// \               array_push($errors,'You must agree to the terms and conditions.');
//            }
            if(count($errors)>0)
            {
                return ['status'=>false,'error'=>$errors];
            }
            else{
                $sql0 = $this->Db->prepare("SELECT * FROM agents WHERE idno=:idno  OR phone = :phone ");
                $sql0->bindParam(':idno', $_POST['idno'], PDO::PARAM_STR);
                $sql0->bindParam(':phone', $newsphone, PDO::PARAM_STR);
                $sql0->execute();
                $cursor0 = $sql0->fetchAll(PDO::FETCH_ASSOC);
                if (sizeof($cursor0) > 0) {
                    array_push($errors,'Phone number or ID number is already in use');
                    if(count($errors)>0)
                    {
                        return ['status'=>false,'error'=>$errors];
                    }
                }
                else{
                    if($sql->execute()){
                        $result = move_uploaded_file($tempFileName2,$fileTarget2);
                        $result1 = move_uploaded_file($tempFileName,$fileTarget);
                        return ['status'=>true,'message'=>'Your application is submitted successfully,kindly wait for confirmation sms'];
                    }
                    else{
                        array_push($errors,'Internal server error');
                    }
                    if(count($errors)>0)
                    {
                        return ['status'=>false,'error'=>$errors];
                    }
                }
            }
        }
}

  public  function getCounties(){
    $sql0 = $this->Db->prepare("SELECT * FROM regionInKenya");
    if($sql0->execute()) {
        $counties=$sql0->fetchAll(PDO::FETCH_ASSOC);
        return $counties;
    }
    else{
        return ['status'=>false,'message'=>'No data was found'];
    }

}



}
