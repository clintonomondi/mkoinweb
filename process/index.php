<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 3/21/2018
 * Time: 1:03 PM
 */
session_start();
header("Content-Type: application/json; charset=UTF-8");
require 'config/constants.php';
if (isset($_SESSION['loggedIn'])) {    //require 'view.php';
    //$views = new view();

    $ch = curl_init(SERVER_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $token = $_SESSION['token'];
    $headers = [
        'token: ' . $token
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    switch ($_POST['action']) {
        case 'initialize':
            successMessage($views->getDefaultUI());
            break;
        case 'paySubCoins':
            echo $views->paySubCoins();
            break;
        case 'suspend':
            echo $views->suspend();
            break;
        case 'fundSubCoins':
            echo $views->fundSubAccount();
            break;
        case 'buyAirtime':
            echo $views->buyAirtimeNow();
            break;
        case 'sendCoins':
            echo $views->sendCoins();
            break;
        case 'sendSubCoins':
            echo $views->sendSubCoins();
            break;
        case 'withCoins':
            echo $views->withCoins();
            break;
        case 'withSubCoins':
            echo $views->withSubCoins();
            break;
        case 'saveCallbackUrl':
            echo $views->saveCallbackUrl();
            break;
        case 'buyToken':
            echo $views->buyTokenNow();
            break;
        case 'createSubAccount':
            echo $views->createSubAccount();
            break;
        case 'getAllSubTransactions':
            echo $views->getAllSubTransactions();
            break;
        case 'subBuyAirtime':
            echo $views->subBuyAirtime();
            break;
        case 'apiKey':
            echo $views->requestApiKey();
            break;
        case 'personalAccountLogin':
            login();
            break;
        case 'personalAccountLogout':
            logout();
            successMessage([]);
            break;
        case 'tumaPesa':
            sendKakitu();
            break;
        case'toaKakitu':
            withdraw();
            break;
        case 'submitEnquiry':
            echo submitEnquiry();
            break;
        default:
            echo json_encode(['status' => false, 'message' => 'Action Requested Not Available']);
            break;
    }

} else {
    if (isset($_POST['action']) AND $_POST['action'] == 'personalAccountLogin') {
        login();
    } else {
        errorMessage(['message' => 'Please Login to continue', 'errorCode' => '000000']);
    }
}

function login()
{
    $data = [
        'action' => 'loginUser',
        'username' => $_POST['username'],
        'token' => '0',
        'password' => $_POST['password']
    ];
    $ch = curl_init(SERVER_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    curl_close($ch);
    $dataBack = json_decode($response, true);
    if ($dataBack['status'] === TRUE) {
        setSessionVariables($dataBack);
        successMessage([]);
        echo $response;
    } else {
        errorMessage(['message' => $dataBack['message']]);
    }
}
function sendKakitu(){
    $data = [
        'action' => 'sendMoney',
        'amount' => intval($_POST['kiasi']),
        'token' => $_SESSION['token'],
        'account' => $_POST['mtumiwa'],
        'byCoin'=>intval($_POST['aina'])
    ];
    $ch = curl_init(SERVER_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    curl_close($ch);
    $dataBack = json_decode($response, true);
    if ($dataBack['status'] === TRUE) {
       successMessage([]);
        //echo $response;
        setSessionVariables($dataBack);
    } else {
        errorMessage(['message' => $dataBack['message']]);
    }
}
function logout()
{
    $_SESSION = array();
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params['path'], $params['domain'], $params['secure'], $params['httponly']);
    }
    session_destroy();
}
function successMessage($message)
{
    $message['status'] = true;
    printMessage($message);
}
function errorMessage($message)
{
    $message['status'] = false;
    printMessage($message);
}
function printMessage($message)
{
    echo json_encode($message);
}
function setSessionVariables($dataBack){
    $_SESSION['loggedIn'] = TRUE;
    $_SESSION['token'] = $dataBack['profile']['mem_id'];
    $_SESSION['name'] = $dataBack['profile']['fname'];
    $_SESSION['phone'] = $dataBack['profile']['AuthorizedTel'];
    $_SESSION['nationalId'] = $dataBack['profile']['idno'];
    $_SESSION['kagina'] = $dataBack['profile']['Wallet'];
    $_SESSION['stimaPoint'] = $dataBack['profile']['kplcPoints'];
    //$_SESSION['allTransactions']=$dataBack['transactions'];
}
function withdraw(){
    $ch = curl_init(SERVER_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $token = $_SESSION['token'];
    $headers = [
        'token: ' . $token
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $postData = [
        'token' => $token,
        'amount' =>$_POST['amountToWithdraw'],
        'action' => 'withdrawMoney'
    ];
    $ch = curl_init(SERVER_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $response = curl_exec($ch);
    curl_close($ch);
    $responseIn = json_decode($response, true);
    if ($responseIn['status'] === TRUE){
        //$dataBack = $this->profile($responseIn['profile']);
        setSessionVariables($responseIn);

        echo json_encode(['status'=>true,'data'=>'transaction successful thank you']);

    }
       else

  echo $response ;
//    $data = [
//        'action' => 'withdrawMoney',
//        'amount' => $_POST['amountToWithdraw'],
//        'token' => $_SESSION['token']
//    ];
//    $ch = curl_init(SERVER_URL);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//    $response = curl_exec($ch);
//    curl_close($ch);
//    $dataBack = json_decode($response, true);
//    if ($dataBack['status'] === TRUE) {
//        successMessage([]);
//        echo $response;
//        setSessionVariables($dataBack);
//    } else {
//        errorMessage(['message' => $dataBack['message']]);
//    }
}



