<?php
session_start();
if(!isset($_SESSION['loggedIn']) && $_SESSION['loggedIn']!=true){
    header("Location:login.php");
}
require_once('navbar.php')
    ?>

<div class="row">
    <div class="col-md-6 col-lg-4">
        <div class="widget-small primary coloured-icon"><i class="icon fa fa-user fa-3x"></i>
            <div class="info">
                <h4>NAME: <?php echo $_SESSION['name']; ?></h4>
                <p>PHONE:<b> <?php echo $_SESSION['phone']; ?></b></p>
                <p>NATIONAL ID:<b> <?php echo $_SESSION['nationalId']; ?></b></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-4">
        <div class="widget-small info coloured-icon"><i class="icon fa fa-money fa-3x"></i>
            <div class="info">
                <h4>KSH (<?php echo $_SESSION['kagina']; ?>)</h4>
                <p><b>Actual balance</b></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-4">
        <div class="widget-small danger coloured-icon"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
                <h4>(<?php echo $_SESSION['stimaPoint']; ?>)</h4>
                <p><b>Kplc Points</b></p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <h3 class="tile-title">Some welcoming message bellow</h3>
            <div class="tile-body">Hey there, I am a very simple card. I am good at containing small bits of information. I am quite convenient because I require little markup to use effectively.</div>
            <div class="tile-footer"></div>
        </div>
    </div>
</div>


<?php
require_once('footer.php');
?>
