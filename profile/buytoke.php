<?php
require_once ('navbar.php');
?>

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <h3 class="tile-title">Buy Token</h3>
                <div class="tile-body">

                    <form>

                        <br/>
                        <div class="form-group">
                            <label class="control-label">Meter No.</label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount.</label>
                            <input class="form-control" type="number">
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" id="optionsRadios1" type="radio" name="optionsRadios" value="option1" checked="">Token
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" id="optionsRadios2" type="radio" name="optionsRadios" value="option2">Postpaid
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="tile-footer">
                            <button class="btn btn-info" type="button"><i class="fa fa-add"></i>Buy Airtime</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
require_once ('footer.php');
?>