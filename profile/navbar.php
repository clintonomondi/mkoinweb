<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>M-Koin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../img/logo3.png" type="image/png">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="doc/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="app sidebar-mini rtl">
<?php
require_once ('modals/send.php');
require_once ('modals/withdraw.php');
require_once ('modals/deposit.php');
require_once ('modals/donate.php');
?>
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="index.php">M-Koin</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->

        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                <li><a class="dropdown-item" href="#" onclick="logot();"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>

            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
<!--        <img class="app-sidebar__user-avatar" src="doc/images/Son.jpeg" alt="" style="height:40px;width:40px;">-->
        <div>
            <p class="app-sidebar__user-name">Username</p>
            <p class="app-sidebar__user-designation">Role</p>
        </div>
    </div>
    <ul class="app-menu">
        <li><a class="app-menu__item active" href="index.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Account</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="#" data-toggle="modal" data-target="#send"><i class="icon fa fa-arrow-circle-o-right"></i> Send</a></li>
                <li><a class="treeview-item" href="#" data-toggle="modal" data-target="#withdraw"><i class="icon fa fa-arrow-circle-up"></i> Withdraw</a></li>
                <li><a class="treeview-item" href="#" data-toggle="modal" data-target="#deposit"><i class="icon fa fa-arrow-circle-down"></i> Deposit</a></li>
                <li><a class="treeview-item" href="#" data-toggle="modal" data-target="#donate"><i class="icon fa fa-circle-o"></i> Donate</a></li>

            </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-book"></i><span class="app-menu__label">Transactions</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="account.php" ><i class="icon fa fa-arrow-circle-o-right"></i> Account statement</a></li>
                <li><a class="treeview-item" href="airtime.php" ><i class="icon fa fa-arrow-circle-up"></i> Airtime</a></li>
                <li><a class="treeview-item" href="token.php"><i class="icon fa fa-arrow-circle-down"></i> KPLC Token</a></li>
                <li><a class="treeview-item" href="shop.php"><i class="icon fa fa-circle-o"></i> Shop</a></li>
                <li><a class="treeview-item" href="paybill.php"><i class="icon fa fa-circle-o"></i> Paybill</a></li>
                <li><a class="treeview-item" href="voucher.php"><i class="icon fa fa-circle-o"></i> Voucher</a></li>

            </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-bitcoin"></i><span class="app-menu__label">M-Koin</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="subaccount.php" ><i class="icon fa fa-arrow-circle-o-right"></i> New account</a></li>
                <li><a class="treeview-item" href="buyairtime.php" ><i class="icon fa fa-arrow-circle-up"></i> Buy Airtime</a></li>
                <li><a class="treeview-item" href="buytoke.php"><i class="icon fa fa-arrow-circle-down"></i> KPLC Token</a></li>
                <li><a class="treeview-item" href="shop.php"><i class="icon fa fa-circle-o"></i> Shop</a></li>
                <li><a class="treeview-item" href="paybill.php"><i class="icon fa fa-circle-o"></i> Paybill</a></li>
                <li><a class="treeview-item" href="voucher.php"><i class="icon fa fa-circle-o"></i> Voucher</a></li>

            </ul>
        </li>
    </ul>
</aside>
<main class="app-content">
    <div class="app-title">
        <div>
            <img src="doc/images/logo.png">
        </div>
    </div>

<!--    <main class="py-4">-->
<!--        @yield('content')-->
<!--    </main>-->

