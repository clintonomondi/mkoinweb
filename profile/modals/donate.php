<!-- The Modal -->
<div class="modal fade" id="donate">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">M-Koin</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form>
                    <h2>Donate coins</h2>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" id="optionsRadios1" type="radio" name="optionsRadios" value="option1" checked="">M-Koin Account
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" id="optionsRadios2" type="radio" name="optionsRadios" value="option2">Mobile
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Mobile No..</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount.</label>
                        <input class="form-control" type="number">
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-info" type="button"><i class="fa fa-arrow-circle-right"></i>Send</button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>