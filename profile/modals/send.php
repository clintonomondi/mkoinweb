<!-- The Modal -->
<div class="modal fade" id="send">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">M-Koin</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form id="tumaMbeca" onsubmit="return false">
                    <h2>Send Coins</h2>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" id="optionsRadios1" type="radio" name="aina" value="1" checked="">M-Koin Account
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" id="optionsRadios2" type="radio" name="aina" value="0">Mobile
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Mobile No..</label>
                        <input class="form-control" type="text" required name="mtumiwa"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount.</label>
                        <input class="form-control" type="text" pattern="\d*" name="kiasi" required/>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-info" type="submit"><i class="fa fa-arrow-circle-right"></i>Send</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>