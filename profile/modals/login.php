<!-- The Modal -->
<div class="modal fade" id="login">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">SignIn</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="bs-component">

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="home">
                            <form>
                                <div class="form-group">
                                    <label class="control-label">Mobile no.</label>
                                    <input class="form-control" type="number">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input  class="form-control" type="password">
                                </div>

                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">

                                        </label>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-arrow-circle-right"></i>Login</button>
                                </div>
                            </form>

                        </div>
                        <!--                        Merchant form-->

                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>