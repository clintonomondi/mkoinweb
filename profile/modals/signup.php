<!-- The Modal -->
<div class="modal fade" id="signup">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Signup</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="bs-component">
                    <ul class="nav nav-tabs  nav-justified">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">Individual</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">Merchant</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="home">
                            <form>
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Mobile No.</label>
                                    <input class="form-control" type="number" >
                                </div>
                                <div class="form-group">
                                    <label class="control-label">ID No/Passport.</label>
                                    <input class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox">I accept the terms and conditions
                                        </label>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>
                                </div>
                            </form>

                        </div>
<!--                        Merchant form-->
                        <div class="tab-pane fade" id="profile">
                            <form>
                                <div class="form-group">
                                    <label class="control-label">Business Name.</label>
                                    <input class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Mobile No.</label>
                                    <input class="form-control" type="number">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Number of Branches.</label>
                                    <input class="form-control" type="number" >
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Location.</label>
                                    <input class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">CR12 & KRA PIN</label>
                                    <input class="form-control" type="file">
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" required>I accept the terms and conditions
                                        </label>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>