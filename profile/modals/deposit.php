<!-- The Modal -->
<div class="modal fade" id="deposit">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">M-Koin</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form>
                    <h2>Deposit Coins</h2>
                    <div class="row justify-content-center">
                        <div class="col-sm-6">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" id="optionsRadios1" type="radio" name="optionsRadios" value="option1" checked="">Express Deposit
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" id="optionsRadios2" type="radio" name="optionsRadios" value="option2">Manual Deposit
                                </label>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <label class="control-label">Phone No.</label>
                        <input class="form-control" type="text" value="phone no here" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount.</label>
                        <input class="form-control" type="number">
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-info" type="button"><i class="fa fa-arrow-circle-down"></i>Deposit</button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>