<!-- The Modal -->
<div class="modal fade" id="withdraw">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">M-Koin</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form id="toaKakitu" onsubmit="return false">
                    <h2>Withdraw Coins</h2>
                    <div class="row justify-content-center">
                        <div class="col-sm-6">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" id="optionsRadios1" type="radio" name="optionsRadios" value="option1" checked="">M-Pesa
                        </label>
                    </div>
                        </div>
                        <div class="col-sm-6">

                    </div>
                    </div>
                        <br/>
                    <div class="form-group">
                        <label class="control-label">Amount.</label>
                        <input class="form-control" type="text" pattern="\d*" name="amountToWithdraw">
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-info" type="submit"><i class="fa fa-arrow-circle-up"></i>Withdraw</button>
                    </div>
                </form>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>