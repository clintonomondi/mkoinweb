<?php
require_once ('navbar.php');
?>

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <h3 class="tile-title">Paybill statement</h3>
                <div class="tile-body">
                    <table class="table table-hover table-bordered responsive" id="sampleTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Pay bill</th>
                            <th>Account</th>
                            <th>Amount </th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                        </tr>
                        <tr>
                            <td>Garrett Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                        </tr>
                        </tbody>
                    </table>

<?php
require_once ('footer.php');
?>