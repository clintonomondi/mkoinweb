<?php
require_once ('navbar.php');
?>

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <h3 class="tile-title">Voucher statement</h3>
                <div class="tile-body">
                    <table class="table table-hover table-bordered responsive" id="sampleTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Balance </th>
                            <th>Beneficiary</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>61</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Garrett Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                            <td>61</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

<?php
require_once ('footer.php');
?>