

<!-- The Modal -->
<div class="modal fade" id="faqs">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-info">
                <h4 class="modal-title">M-KOIN</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">


                <div class="container">
                    <div class="section-header">
                        <h3>FAQ</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <h5><a href="#" data-toggle="collapse" data-target="#demo1">1. What is M-Koin?</a></h5>
                                <div id="demo1" class="collapse">
                                    M-Koin is a platform for cashless loose change transactions and also payment of fare to Matatus.
                                    It also allows users to buy airtime, Kenya Power tokens and also vouchers.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo2">2. How do I use M-Koin?</a></h5>
                                <div id="demo2" class="collapse">
                                    To use M-Koin, one must install the app from Google Play Store or use it via USSD code *626#. After that you register your account using your phone number and ID number.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo3">3. What are the charges for using M-Koin?</a></h5>
                                <div id="demo3" class="collapse">
                                    M-Koin is free to register and requires no payment information such as bank cards but your service providers will charge you for internet connection and for USSD it charges 1 Kenyan shilling every time you request the service.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo4">4. During registration, what do I put as referral code?</a></h5>
                                <div id="demo4" class="collapse">
                                    If you have been referred by someone, he/she will provide a code that identifies him but if you don’t have a code just
                                    leave the input blank or put 0 as reference code.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo5">5. What do I do if I have forgotten my pin?</a></h5>
                                <div id="demo5" class="collapse">
                                    On the login screen, you will see a placeholder written FORGOT YOUR PASSWORD?, click on it and it will ask for your ID Number. You will receive an SMS with a code to verify that it is the right person who is requesting for the pin change.
                                    Then you will be asked to enter your new password twice for confirmation purposes.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo6">6. How do I change my name or pin?</a></h5>
                                <div id="demo6" class="collapse">
                                    To change your name or pin, click on the three horizontal bar at the top right of the app and select
                                    Settings then you can easily change your username and pin.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo7">7. How will I know if someone sends me money?</a></h5>
                                <div id="demo7" class="collapse">
                                    When someone sends money/coins to your M-Koin wallet, you will receive an SMS from M-Koin confirming the transaction.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo8">8. How do I send money/coins to someone else?</a></h5>
                                <div id="demo8" class="collapse">
                                    To send money/coins to someone else you must login to your account and press on send coins button on your dashboard then enter his/her
                                    phone number and the amount to transfer then press send coin button. A confirmation pop up will appear with
                                    the details of the transaction, press okay and the coins/money will be sent to that phone number. This service is free of charge.
                                </div>
                                <h5><a href="#" data-toggle="collapse" data-target="#demo9">9. Can I send coins/money to an unregistered number? </a></h5>
                                <div id="demo9" class="collapse">
                                    Yes you can. The unregistered number will receive the coins/money but will not be able to access it unless they register
                                    to the M-Koin Platform.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo10">10. How do I withdraw from my M-Koin wallet? </a></h5>
                                <div id="demo10" class="collapse">
                                    To withdraw from your M-koin wallet you need to press the withdraw button on the dashboard and enter the amount you wish to
                                    withdraw and press okay when the confirmation pop up is shown. The amount will be
                                    accredited to your MPESA wallet or to your Bank account. Please note that you will be charged as mention in table 1.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo11">11. How do I deposit to my M-Koin Account? </a></h5>
                                <div id="demo11" class="collapse">
                                    You have two options when you want to deposit to your account, one is express deposit where you deposit straight from your app using MPESA
                                    STK Push functionality by pressing deposit button on the dashboard screen and don’t have to close the app to do so; the second one is to go to your MPESA menu and go to Lipa na MPESA option the Pay Bill and enter 497497 as business number the enter your M-Koin number as your account number
                                    and the amount to deposit then enter your pin and send. Your M-Koin wallet will be automatically credited with the amount you deposited.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo12">12. How do I view all my transactions? </a></h5>
                                <div id="demo12" class="collapse">
                                    To view all your transactions press on the statement button and a mini statement will appear. To get more details press View More button
                                    at the bottom of the mini
                                    statement and all your transactions will appear.
                                </div>


                                <h5><a href="#" data-toggle="collapse" data-target="#demo13">13.How do I pay fare? </a></h5>
                                <div id="demo13" class="collapse">
                                    To pay fare, press the Lipa Fare button on the dashboard and enter the Matatu Number e.g. 1M and the
                                    amount to pay and click send and then confirm the payment. The conductor will receive the payment
                                    to his account and you will be able to see the transaction on your statement.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo14">14.How do I buy airtime? </a></h5>
                                <div id="demo14" class="collapse">
                                    To buy airtime just press the Buy Airtime button on the bottom of the dashboard screen and enter the amount to buy then
                                    press send. To buy for other phone, just enter the mobile number and the amount and then press send.
                                </div>


                            </div>




                            <div class="col-sm-6">
                                <h5><a href="#" data-toggle="collapse" data-target="#demo15">15.Can I buy airtime for any network?</a></h5>
                                <div id="demo15" class="collapse">
                                    Yes you can buy airtime for any network in Kenya e.g. Safaricom, Airtel or Telkom.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo16">16. How do I buy Kenya Power tokens?</a></h5>
                                <div id="demo16" class="collapse">
                                    Press on the KPLC token button at the bottom of the dashboard screen and then enter your meter number then the amount you wish to purchase and confirm the details and then press okay. You will receive a message with the number of units received. Please note that all the meters that you have used will be
                                    saved for future use. You can also view all your previous tokens history when you press the KPLC tokens button and go to History tab.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo17">17. What is a Sub Account?</a></h5>
                                <div id="demo17" class="collapse">
                                    Sub Account is an account that is a part from the main account and performs different transactions
                                    from the main account and also has a different purpose from the main account. It can be for a Matatu, Chama or Sacco and many other things.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo18">18. How do I create a sub account?</a></h5>
                                <div id="demo18" class="collapse">
                                    To create a sub account, press the +Sub Account button at the bottom of the dashboard screen and then press the Add New button and choose the type of sub account to create and then add
                                    a description of the account and press create. Your new Sub account is ready to use.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo19">19. How do I fund my sub account?</a></h5>
                                <div id="demo19" class="collapse">
                                    To fund your sub account press the +sub account button at the bottom of the dashboard screen and a new
                                    screen will appear with all your sub accounts and at the far right of the sub account you will see an Add Fund button, press it and enter the amount to fund the sub account and press Fund button and confirm the transaction and your sub account will be credited with that amount. Please note that you can only
                                    fund your sub account from your main account. It is free to fund your sub account.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo20">20. How can I view all my sub account transactions?</a></h5>
                                <div id="demo20" class="collapse">
                                    To view your sub account transactions, press the three horizontal bars at the top of the main account dashboard and a dropdown will appear with an option to switch accounts, press Switch option and select the sub account that you want and a dashboard of the sub account will appear with a mini statement, send coins and withdraw coins options. To view all the transactions of
                                    a sub account press View More at the far right of the mini statement header and all your transactions will appear.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo21">21. How can I send coins from a sub account?</a></h5>
                                <div id="demo21" class="collapse">
                                    The process of sending coins from a sub account is similar to that of the main account. Please note that you cannot send
                                    coins/money from your sub account to your main account.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo22">22. How can I withdraw from sub account?</a></h5>
                                <div id="demo22" class="collapse">
                                    You can withdraw from sub account in the same way you withdraw from the main account. Please note that the charges are similar to that of the main account which are mentioned in the table 1.
                                </div>
                                <h5><a href="#" data-toggle="collapse" data-target="#demo23">23. Are my login credentials the same for USSD and App? </a></h5>
                                <div id="demo23" class="collapse">
                                    Yes the details you entered during registration on any of the two platforms are similar. You can use the same details to login to any of them.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo24">24. Is my data safe and private? </a></h5>
                                <div id="demo24" class="collapse">
                                    We take safety and privacy very seriously,therefore we will not share any details of your transactions or any
                                    other information you provide us with any other party. We have encrypted all your data on our secure servers.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo25">25. Can I deposit from my bank account to my M-Koin account? </a></h5>
                                <div id="demo25" class="collapse">
                                    Yes you can deposit from your bank account to M-Koin using Pesalink.
                                </div>

                                <h5><a href="#" data-toggle="collapse" data-target="#demo26">26. Is M-Koin a product of Sidian Bank? </a></h5>
                                <div id="demo26" class="collapse">
                                    M-Koin is not a product of Sidian Bank but we have partnered with them as a trusted financial services provider to
                                    secure our users' transactions.
                                </div>


                                <h5><a href="#" data-toggle="collapse" data-target="#demo27">27.How can I contact M-Koin? </a></h5>
                                <div id="demo27" class="collapse">
                                    <h6>You can use the following information to contact us:</h6>
                                    <h6>Email: app@mkoin.co.ke</h6>
                                    <h6>Phone: +254 745 411339/+254 721 204384</h6>
                                    <h6>Website: https://www.mkoin.co.ke</h6>
                                    <h6>Facebook: M-Koin</h6>
                                    <h6>Twitter: @MkoinKe</h6>


                                </div>
                            </div>
                        </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>