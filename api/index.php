<?php
header("Content-Type: application/json; charset=UTF-8");
if (isset($_POST['action']) || isset($_GET['action'])) {
    if (isset($_POST['action']))
        $action = $_POST['action'];
    else
        $action = $_GET['action'];
    require '../engine/index.php';
    try {

        $Cilcom = new index();
        switch ($action) {
            case "submitEnquiry":
                echo json_encode($Cilcom->submitEnquiry());
                break;
            case "submitMerchant":
                echo json_encode($Cilcom->submitMerchant());
                break;
            case 'getCounties':
                echo json_encode($Cilcom->getCounties());
                break;
            default:
                echo json_encode(['status' => false, 'message' => 'Invalid request: ' . $action . ' is not found']);
                break;
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }

}

